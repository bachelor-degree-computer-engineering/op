#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int main(){
  pid_t pid;

  //genero un nuovo processo
  pid = fork();

  if(pid < 0){
    fprintf(stderr, "generazione del nuovo processo fallita.\n");
    return -1;
  }
  else if(pid == 0){ //processo figlio
    execlp("/bin/ls", "ls", NULL);
  }
  else{ //processo genitore
    wait(NULL);
    printf("processo figlio ha terminato\n");
  }
  return 0;
}
