#include <stdio.h>
#include <unistd.h>

int main(){
  pid_t pid = 0;
  int i = 0;
  while(pid != -1){
    pid = fork();
    if(pid == 0){while(1);}
    i++;
  }
  printf("%d processi creati", i);
  return 0;
}
