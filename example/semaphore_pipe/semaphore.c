#include <unistd.h>

void semaphoreInit( int *S){
   if(pipe(S) == -1) {
      printf("Init Error\n");
      exit(-1);
   }
   return;
}

void semaphoreSignal( int *S){
   char ctr = 'X';
   if(write(S[1], &ctr, sizeof(char)) != 1){
      printf("Signal error\n");
      exit(-1);
   }
   return;
}

void semaphoreWait( int *S ){
   char ctr;
   if(read(S[0], &ctr, sizeof(char)) != 1){ // -> questa read è bloccante.
      printf("Wait error\n");
      exit(-1);
   }
   return;
}