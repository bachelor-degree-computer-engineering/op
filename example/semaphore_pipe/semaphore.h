#ifndef SEMAPHORE_H
#define SEMAPHORE_H

void semaphoreInit(int *S);
void semaphoreSignal(int *S);
void semaphoreWait(int *S);

#endif