#!/bin/bash

fileName=$1 # filename ricevuto da linea di comando
i=0
for line in $(cat $fileName)
do
  filename=$(basename $fileName.txt)
  nc=$(echo $line | wc -c)
  echo $nc >> tmp.txt
  echo "$line $nc" > $filename".out"
done

echo $(sort -r -n tmp.txt | head -n 1)
echo $(wc -l $1)
