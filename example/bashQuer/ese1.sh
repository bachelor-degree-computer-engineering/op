#!/bin/bash

# 1 - nome di una directory
# 2 - nome di una funzione
# 3 - file di uscita


if [ $# -ne 3 ]
then
    echo "Wrong number of argument"
    exit 1
fi

for file in $(find $1 -type f)
do
    grep -ne "$2" $file --silent

done
