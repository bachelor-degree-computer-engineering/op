#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#define MAX 5

waitRandomTime(){
  sleep( (int) rand() % MAX + 1);
}

void* t1(){
  waitRandomTime();
  printf("C\n");
  waitRandomTime();
  printf("E\n");
  pthread_exit(NULL);
}

void* G(){
  waitRandomTime();
  printf("G\n");
  pthread_exit(NULL);
}

void* t2(){
  pthread_t tid;
  waitRandomTime();
  printf("D\n");
  pthread_create(&tid, NULL, G, NULL);
  waitRandomTime();
  printf("F\n");
  pthread_join(tid, NULL);
  waitRandomTime();
  printf("H\n");
  pthread_exit(NULL);
}

int main(){
  pthread_t tid1, tid2;
  srand(NULL);
  waitRandomTime();
  printf("A\n");

  pthread_create(&tid1, NULL, t1, NULL);
  pthread_create(&tid2, NULL, t2, NULL);
  
  waitRandomTime();
  printf("B\n");

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);

  waitRandomTime();
  printf("I\n");
  return 0;
}
