#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
  pid_t pid;
  int i, j, n, status;

  n = atoi(argv[1]);

  for(i = 0; i < n; i++){
    pid = fork();
    if(pid == 0){
      for(j = 0; j < 10; j++){
        printf("%c\n",'a'+i);
      }
      exit(0);
    }
    else if(pid > 0){
      waitpid(pid, &status, 0);
    }
    printf("\n");
  }
}
