#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
  pid_t pid;
  int i = 1;
  while(argv[i] != NULL){
    pid = fork();
    if(pid == 0){
      execl("./fibonacci", "fibonacci", argv[i], NULL);
    }
    i++;
  }
}
