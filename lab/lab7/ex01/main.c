#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void *threadFunction1(void* args){
  printf("Elaborazione del primo thread\n");
  sleep(5);
  printf("Il primo thread termina\n");
  pthread_exit(NULL);
}

void *threadFunction2(void* args){
  printf("Elaborazione del secondo thread\n");
  sleep(5);
  printf("Il secondo thread termina\n");
  pthread_exit(NULL);
}

int main(){
  pthread_t thread_ID1, thread_ID2;
  int return1, return2;

  return1 = pthread_create(&thread_ID1, NULL, &threadFunction1, NULL);
  return2 = pthread_create(&thread_ID2, NULL, &threadFunction2, NULL);
  if(!return1)
    printf("Primo thread creato\n");
  if(!return2)
    printf("Secondo Thread creato\n");
  sleep(3);
  return 0;
}
