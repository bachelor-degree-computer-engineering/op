#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[]) {
  int i;
  char str[100];
  fork();
  for(i=0; i <2; i++){
    if(fork()!=0){
      sprintf(str, "echo system with i=%d", i);
      system(str);
    }else{
      sprintf(str, "exec with i=%d", i);
      execlp("echo", "myPgrm", str, NULL);
    }
  }
  return 0;
}
