#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
  pid_t pids[6];
  pid_t returned;
  int flag[6];
  int i,j, n;
  srand((unsigned) time(NULL));
  for(i=0; i < 3; i++){
    flag[i] = 0;
  }
  for(i=3; i < 6; i++){
    flag[i] = 1;
  }
  // 0 type 1 print forever random numbers.
  // 1 type 2 print 50 random numbers.

  // Generation of process
  for(i = 0; i < 6; i++){
    pids[i] = fork();
    if(pids[i] == 0){
      // Son
      switch(flag[i]){
        case 0:
          while(1){
            printf("1 - %d\n", rand() % 100);
            sleep(1);
          }
          exit(0);
        break;
        case 1:
          for(j=0; j < 50; j++){
            n = rand() % 100;
            printf("2 - %d, %d\n", n, n*n);
            sleep(1);
          }
          exit(1);
        break;
      }
    }
  }
  // Forever: recreate ended process.
  while (1) {
    returned = wait(NULL);
    for(i=0; i < 6; i++){
      if(pids[i] == returned){
        pids[i] = fork();
        if(pids[i] == 0){
          switch(flag[i]){
            case 0:
              while(1){
                printf("1 - %d\n", rand() % 100);
              }
            break;
            case 1:
              for(j=0; j < 50; j++){
                n = rand() % 100;
                printf("2 - %d, %d\n", n, n*n);
              }
              exit(1);
            break;
          }
        }else if(pids[i] > 0){
          break;
        }
      }
    }
  }
  return 0;
}
