#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
  pid_t pid;
  int  i = 0;
  fprintf(stdout, "Processo padre, con PID = %d, PPID =%d\n", getpid(), getppid());
  for(int i = 0; i < 3; i++){
    pid = fork();
    if(pid == -1){
      fprintf(stderr, "Errore nella generazione dei processi\n");
    }else if(pid > 0){
      //processo padre, non faccio niente.
    }else if(pid == 0){
      // sono un processo figlio
      fprintf(stdout, "Sono un processo figio %d-esimo, con PID = %d e padre = %d\n",i, getpid(), getppid());
      fprintf(stdout, "Io termino qui\n" );
      exit(0);
    }
  }
}
