#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
  pid_t pid;
  int i = 0 ;

  for(i = 0; i < 3; i++){
    pid = fork();

    if (pid == -1){
      fprintf(stderr, "Errore nella generazione dei procssi.\n");
    }else if(pid > 0){
      fprintf(stdout, "Sono il padre, PID = %d, termino qui\n", getpid());
      exit(0);
    }else if(pid == 0){
      fprintf(stdout, "Sono il figlio %d, PID = %d\n",i, getpid());
    }
  }
}
