#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

typedef struct{
  char *in, *out;
}file_t;

void readFileIn(FILE *in, int *v, int n){
  int i;
  for(i = 0; i < n; i++){
    fscanf(in, "%d", &v[i]);
  }
  printf("Stampo vettore per controllo:\n");
  for(i = 0; i < n; i++){
    printf("%d ", v[i]);
  }
  printf("\n");
  return;
}

void sort(int *v, int n){
  int i, j, tmp;
  for(i=0; i < n; i++){
    for(j=0; j < n - i; j++){
      if(v[j] > v[j+1]){
        tmp = v[j];
        v[j] = v[j+1];
        v[j+1] = tmp;
      }
    }
  }
  return;
}

void writeFileOut(FILE *out, int *v, int n){
  int i;
  for(i = 0; i < n; i++){
    fprintf(out, "%d\n", v[i]);
  }
  return;
}

void* threadFunction(void* args){
  printf("Entrato nel thread");
  file_t* localCopy;
  int *v, n;
  char *in, *out;
  FILE *inF, *outF;
  localCopy = args;
  in = strdup(localCopy->in);
  out = strdup(localCopy->out);
  printf("Sono un thread i mie file sono: %s %s\n", in, out);
  inF = fopen(in, "r");
  outF = fopen(out, "w");

  fscanf(inF,"%d",&n);
  printf("Dimensione vettore %d\n", n);
  v = malloc(n * sizeof(int));

  readFileIn(inF, v, n);
  sort(v, n);
  printf("Stampo vettore ordinato per controllo:\n");
  for(int i = 0; i < n; i++){
    printf("%d ", v[i]);
  }
  printf("\n");
  writeFileOut(outF, v, n);

  pthread_exit(NULL);
}

int main(int argc, char* argv[]){
  int i, numFiles, err;
  pthread_t tid[argc/2];
  file_t fileStruct[argc/2];
  // ciclo for sui vari file
  printf("argc: %d\n", argc);
  numFiles = argc/2;
  printf("numFiles: %d\n", numFiles);
  for(i = 0; i < numFiles; i++){
    fileStruct[i].in  = strdup(argv[1 + i*2]);
    fileStruct[i].out = strdup(argv[2 + i*2]);
    printf("Copiati in sicurezza in(%s) e out(%s)\n", fileStruct[i].in, fileStruct[i].out);
    err = pthread_create(&tid[i], NULL, threadFunction, (void *) &fileStruct[i]);
    if(err == 0){
      printf("Thread creato\n");
    }
  }
  for(i = 0; i < numFiles; i++){
    pthread_join(tid[i], NULL);
  }
  return 0;
}
