#!/bin/bash

# Controllare ogni X secondi se utente U
# ha effettuato logout

if [ $# -ne 2 ]
then
  echo "Argument error"
  exit 1
fi

X=$1 #secondi
U=$2 #utente

while [ 1 -eq 1 ]
do
  out=$(who | grep $X)
  echo $out
  if [ $? -ne 0 ]
  then
    echo "User $U not logged"
  else
    echo "User $U logged"
  fi

  sleep $X
done
