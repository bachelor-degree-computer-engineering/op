/*   matrixReading.c
 *   @author  : Gabriele Iurlaro
 *   @  site  : http://iurlaro.com
 *   @Modified: 1/1/2021, 15:18:30
*/

#include <stdio.h>
#include <pthread.h>

#define R 3
#define C 3

// A global matrix

int matrix[R][C];

typedef struct {
  int rowNumber;
}thread_args;

void *rowReader(void *args){
  thread_args* data = (thread_args *) args;
  int i = 0;
  for( i = 0; i < C; i++){
    scanf("%d", &matrix[data -> rowNumber][i]);
  }
  pthread_exit(NULL);
}

void *rowWriter(void *args){
  int i;
  thread_args* data = (thread_args* ) args;
  for( i = 0 ; i < C; i++){
    printf("%d ", matrix[data -> rowNumber][i]);
  }
  printf("\n");
  pthread_exit(NULL);
}

int main(){
  pthread_t tids[R];
  int i;
  thread_args argument[R];
  // readers thread
  printf("============\n" );
  printf("READING BY ROW\n");
  for( i = 0; i < R; i++){
    argument[i].rowNumber = i;
    pthread_create(&tids[i], NULL, rowReader, &argument[i]);
    pthread_join(tids[i], NULL);
  }
  printf("\n===========\n");
  printf("PRINTING BY ROW\n");
  // writer thread
  for ( i=0; i < R; i++){
    pthread_create(&tids[i], NULL, rowWriter, &argument[i]);
    pthread_join(tids[i], NULL);
  }
  pthread_exit(NULL);
}
